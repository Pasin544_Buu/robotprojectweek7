/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

/**
 *
 * @author Pla
 */
public class Fuel extends Obj{
    int volum;
    public Fuel(int x, int y,int volum) {
        super('F', x, y);
        this.volum=volum;
    }
    
    public int fillFuel(){
        int vol= volum;
        symbol = '-';
        volum = 0;
        return vol;
    }
}
